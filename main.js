document.addEventListener('DOMContentLoaded', function () {
    fetch('data.json')
        .then(response => response.json())
        .then(data => buildTreeView(data))
        .catch(error => console.error('Ошибка загрузки данных:', error));

    function buildTreeView(data) {
        const container = document.getElementById('treeView');
        const tree = buildTree(data.services);
        container.appendChild(tree);
    }

    function buildTree(data, head = null) {
        const node = document.createElement('ul');
        node.className = 'tree-node';

        data.filter(item => item.head === head).sort((a, b) => a.sorthead - b.sorthead).forEach(item => {
            const li = document.createElement('li');
            li.textContent = item.name + ' (' + item.price + ')';
            li.className = item.node === 1 ? 'tree-node-toggle' : 'tree-leaf';
            li.id = 'service-' + item.id;
            li.setAttribute('data-type', item.type);

            if (item.node === 1) {
                li.addEventListener('click', function (e) {
                    e.stopPropagation();
                    const isExpanded = this.classList.toggle('expanded');
                    if (isExpanded) {
                        showChildren(this);
                    } else {
                        hideChildren(this);
                    }
                });
                const childNode = buildTree(data, item.id);
                childNode.classList.add('hidden');
                node.appendChild(li);
                node.appendChild(childNode);
            } else {
                node.appendChild(li);
            }
        });

        return node;
    }

    function showChildren(node) {
        const nextElement = node.nextElementSibling;
        if (nextElement && nextElement.tagName === 'UL') {
            nextElement.classList.remove('hidden');
        }
    }

    function hideChildren(node) {
        const nextElement = node.nextElementSibling;
        if (nextElement && nextElement.tagName === 'UL') {
            nextElement.classList.add('hidden');
        }
    }
});